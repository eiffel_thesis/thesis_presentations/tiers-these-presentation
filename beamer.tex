\documentclass[table]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{default}
\usepackage[english]{babel}
\usepackage{color}
\usepackage{siunitx}
\usepackage{pgfplots}
\usepackage{algpseudocode}
\usepackage{rotating}
\usepackage{bytefield}
\usepackage{graphicx}
\usepackage{diagbox}
\usepackage{subfig}
\usepackage{pifont}

\let\pgfmathMod=\pgfmathmod\relax

\sisetup{binary-units=true}

\graphicspath{{vectors/EPS/}{vectors/PDF/}}

\usetheme{Rochester}

\definecolor{struct}{HTML}{03a9f4}
\definecolor{alert}{HTML}{f44336}
\definecolor{example}{HTML}{aeea00}
\definecolor{struct2}{HTML}{8bc34a}
\definecolor{amber}{HTML}{ff9800}
\definecolor{ppink}{HTML}{e91e63}
\definecolor{violet_profond}{RGB}{103, 58, 183}
\definecolor{sarcelle}{RGB}{0, 150, 136}

\setbeamercolor{structure}{fg = struct}
\setbeamercolor{normal text}{fg = white, bg = black}
\setbeamercolor{example text}{fg = example}
\setbeamercolor{alerted text}{fg = alert}
\setbeamercolor{footline}{fg = white}

\setbeamertemplate{navigation symbols}

\setbeamerfont{page number in head/foot}{size=\small}
\setbeamertemplate{footline}[frame number]

\pgfkeys{/pgf/number format/1000 sep=}

% \pgfplotscreateplotcyclelist{mycolorlist}{%
% 	{draw = example, fill = example},
% 	{draw = alert, fill = alert},
% 	{draw = struct, fill = struct}
% }

\title{\textit{L'oisif ira loger ailleurs}: Towards memory consolidation based on applicative feedback from containers}
\author{Francis Laniel\\ \small{Jonathan Lejeune, Julien Sopena, Marc Shapiro and Franck Wajsburt}}
\date{June 2019}
% \logo{\includegraphics[scale = .07]{LIP6.eps}}

\begin{document}
	\setbeamertemplate{caption}{\raggedright\insertcaption\par}

	\frame{\titlepage}

	\begin{frame}
		\frametitle{Two topics}

		\begin{enumerate}
			\item Towards an efficient use of non volatile memory to reduce energy consumption (month 0 to 15):
			Reduce DRAM power consumption.
			\item \textit{L'oisif ira loger ailleurs}: Towards memory consolidation based on applicative feedback from containers (month 15 to now):
			Consolidate memory for containers.
		\end{enumerate}
	\end{frame}

	\begin{frame}
		\frametitle{Datacentre energy consumption}
		\framesubtitle{Focus on DRAM}

		\begin{columns}[T]
			\begin{column}{.8\textwidth}
				\begin{figure}[H]
					\centering

					\begin{tikzpicture}[scale = .75]
						\begin{axis}[xlabel = Time (in year),
							ylabel = Percentage of whole energy consumption]
							\addplot+[only marks, color=struct, mark=x] coordinates{
								(2003, 20)
								(2003, 40)
								(2006, 22)
								(2009, 20)
								(2009, 30)
								(2010, 12)
								(2010, 15)
								(2010, 23)
								(2010, 33)
								(2013, 14.06)
								(2015, 5)
							};
						\end{axis}
					\end{tikzpicture}

					\caption{DRAM energy consumption over time from different studies \cite{minas_problem_2009, wallace_measuring_2013, lefurgy_energy_2003, barroso_datacenter_2009, david_rapl_2010, peyman_blumstengel_leveraging_2010, laudon_ultrasparc_2006, melendez_need_2015}}
				\end{figure}
			\end{column}

			\begin{column}{.2\textwidth}<2>
				\begin{center}
					On average DRAM consumes $\approx 21.28\%$ of the whole energy consumption!

					We want to reduce this proportion.
				\end{center}
			\end{column}
		\end{columns}
	\end{frame}

	\begin{frame}
		\frametitle{DRAM context}
		\framesubtitle{Abstraction of DRAM cell}

		\begin{columns}[T]
			\begin{column}{.6\textwidth}
				\begin{figure}[H]
					\centering

					\includegraphics[scale = .4]{bucket.pdf}

					\caption{Abstraction of a DRAM cell}
				\end{figure}
			\end{column}

			\begin{column}{.43\textwidth}
				\begin{algorithmic}
					\If{$bucket.full()$}
						\State \Return $1b$
					\EndIf
					\If{$bucket.empty()$}
						\State \Return $0b$
					\EndIf
				\end{algorithmic}
			\end{column}
		\end{columns}

	\end{frame}

	\begin{frame}
		\frametitle{Our experiments}
		\framesubtitle{Principle}

		We want to verify if DRAM consumes $\approx 20\%$ of the whole power consumption.

		\visible<2>{
			Principle:
			\begin{itemize}
				\item Vary the number of threads.
				\item Vary the size of allocated memory.
				\item Access memory buffer either sequentially or randomly.
			\end{itemize}

			$Access = read + write + flush\_cache\_line$
		}
	\end{frame}

	\begin{frame}
		\frametitle{Results}
		\framesubtitle{DRAM, CPU and computer power consumption for different experiments}

		\begin{figure}[H]
			\subfloat[Sequential manipulations (29 threads, G5K)]{
				\centering

				\begin{tikzpicture}[scale = .3]
					\begin{axis}[symbolic x coords = {20, 40, 60, 80, 90}, xtick = data, xlabel = Percent of whole memory allocated,
						ylabel = Power (in \si{\W}), ybar = 0, legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
						\addplot [color=alert, fill] coordinates{
							(20, 6.68627000180708)
							(40, 6.56969521103209)
							(60, 8.83751686518943)
							(80, 9.80093732852149)
							(90, 9.93388330883093)
						};

						\addplot [color=struct, fill] coordinates{
							(20, 65.2187558125689)
							(40, 64.8810512372323)
							(60, 66.7821836160273)
							(80, 68.41738542155)
							(90, 68.1736054764683)
						};

						\addplot [color=amber, fill] coordinates{
							(20, 146.373300905736)
							(40, 145.92992358804)
							(60, 150.612900065317)
							(80, 154.195827430779)
							(90, 154.655044757033)
						};

						\legend{DRAM, CPU, Computer}
					\end{axis}
				\end{tikzpicture}
			}
			\subfloat[Random manipulations (29 threads, G5K)]{
				\centering

				\begin{tikzpicture}[scale = .3]
					\begin{axis}[symbolic x coords = {20, 40, 60, 80, 90}, xtick = data, xlabel = Percent of whole memory allocated,
						ylabel = Power (in \si{\W}), ybar = 0, legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
						\addplot [color=alert, fill] coordinates{
							(20, 6.56000822100557)
							(40, 6.75068666049818)
							(60, 7.78756921735714)
							(80, 7.92059648970627)
							(90, 7.90250217967924)
						};

						\addplot [color=struct, fill] coordinates{
							(20, 73.0769351016195)
							(40, 70.7765219363753)
							(60, 69.6853522572062)
							(80, 69.4766063833397)
							(90, 69.1476207098054)
						};

						\addplot [color=amber, fill] coordinates{
							(20, 154.632115320147)
							(40, 151.560817880795)
							(60, 152.726818033323)
							(80, 152.401861513687)
							(90, 152.033903452685)
						};

						\legend{DRAM, CPU, Computer}
					\end{axis}
				\end{tikzpicture}
			}
			\subfloat[Sequential manipulations (160 threads, PowerEdge)]{
				\centering

				\begin{tikzpicture}[scale = .3]
					\begin{axis}[symbolic x coords = {20, 40, 60, 80, 90}, xtick = data, xlabel = Percent of whole memory allocated,
						ylabel = Power (in \si{\W}), ybar = 0, legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
						\addplot [color=alert, fill] coordinates{
							(20, 45.5694627275169)
							(40, 54.1380153734639)
							(60, 57.5373834295119)
							(80, 61.3670048504126)
							(90, 59.8634059452079)
						};

						\addplot [color=struct, fill] coordinates{
							(20, 408.25998790568)
							(40, 415.087024612316)
							(60, 414.09823030002)
							(80, 412.706220277531)
							(90, 408.311802282951)
						};

						\legend{DRAM, CPU}
					\end{axis}
				\end{tikzpicture}
			}
			\subfloat[Random manipulations (160 threads, PowerEdge)]{
				\centering

				\begin{tikzpicture}[scale = .3]<2->
					\begin{axis}[xtick = data, xlabel = Percent of whole memory allocated,
						ylabel = Power (in \si{\W}), ybar = 0, legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
						\addplot [color=alert, fill] coordinates{
							(20, 47.4708890941173)
							(40, 49.7519017835698)
							(60, 50.2270286914457)
							(80, 51.1217587577262)
						};

						\addplot [color=struct, fill] coordinates{
							(20, 447.443224005386)
							(40, 446.287111315575)
							(60, 440.172885755491)
							(80, 430.720891273689)
						};

						\legend{DRAM, CPU}
					\end{axis}
				\end{tikzpicture}
			}
		\end{figure}

		DRAM consumes at least $\approx 10\%$ of the CPU.
	\end{frame}

	\begin{frame}
		\frametitle{Allocate page per rank}

		It is possible to win $\approx 10\%$ of the power consumption!

		\visible<2->{
			A rank is a group of chip which is accessed during read or write.

			Linux allocates physical memory without knowing about ranks.

			Ideas:
			\begin{itemize}
				\item Allocate pages from rank until all pages from rank are full.
				\item Allocate pages from a new rank only if necessary.
				\item Maximize rank idle time so memory controler puts it in low-power mode.
			\end{itemize}

			By doing so, we will reduce energy consumption.
		}

		\only<3>{\textcolor{alert}{It is hard to do so because how a physical address translates to a rank is undocumented.}}
	\end{frame}

	\begin{frame}
		\frametitle{Conclusion}

		\begin{itemize}
			\item DRAM does not consume 20\% of the whole power consumption.
			\item But on average it consumes 10\% of the CPU power consumption.
			\item It is hard to know how physical address translates to rank (undocumented and need reverse-engineering).
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Two topics}

		\begin{enumerate}
			\item Towards an efficient use of non volatile memory to reduce energy consumption.
			\item \textit{L'oisif ira loger ailleurs}: Towards memory consolidation based on applicative feedback from containers .
		\end{enumerate}
	\end{frame}

	\begin{frame}
		\frametitle{From virtual machine to containers \cite{ovh_ovh_nodate, alibaba_alibaba_nodate, amazon_amazon_nodate, microsoft_microsoft_nodate}}

		\begin{figure}
			\centering

			\begin{columns}
				\begin{column}{.3\textwidth}
					\includegraphics<1->[scale = .15]{mem-fig1.pdf}
				\end{column}

				\begin{column}{.3\textwidth}
					\includegraphics<2->[scale = .15]{mem-fig2.pdf}
				\end{column}
				\begin{column}{.3\textwidth}
					\includegraphics<3->[scale = .15]{mem-fig3.pdf}
				\end{column}
			\end{columns}

			\begin{itemize}
				\item<4-> Deployement: software packaging, \texttt{docker hub} \cite{docker_docker_nodate}.
				\item<4-> Security: \texttt{namespace}, capabilities \cite{kerrisk_lce_2012}.
				\item<4-> \textbf{Resource management: \texttt{cgroup} \cite{hiroyu_cgroup_2008}}.
			\end{itemize}

			\begin{description}
				\item<5->[Consolidation:] Multiplex multiple application servers on a single physical machine/
				\item<5->[Performance isolation:] Ensure that consolidation application will not suffer from others.
				\item<6>[Memory consolidation:] Take unused memory from on container to give it to another so it can increase its performance.
				\item<6>[Memory isolation:] Ensure a certain amount of memory to containers.
			\end{description}
		\end{figure}
	\end{frame}

	\begin{frame}
		\frametitle{Experiments}
		\framesubtitle{Question}

		Do current memory limiting mechanisms of Linux permit memory consolidation and/or memory isolation?

		\visible<2->{Experimental settings:}
		\begin{itemize}
			\item<2-> Two containers A and B.
			\item<2-> They will each access DB of \SI{4}{\giga\byte} in a VM with \SI{3}{\giga\byte}.
			\item<2-> They will alternate between high and low activity periods.
		\end{itemize}
	\end{frame}

% This need the pifont package.
% Information was taken from: https://tex.stackexchange.com/a/42620.
\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%

	\begin{frame}
		\frametitle{Experiments}

		\begin{columns}
			\begin{column}{.3\textwidth}
				\centering

				\textbf{No limits}

				\begin{figure}[H]
					\begin{tikzpicture}[scale = .45]
						\begin{axis}[ymax = 1100, error bars/y dir = both, xlabel = Time (second), ylabel = Transactions per second, legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
							\addplot[example] coordinates {(0, 800) (1080, 800)};
							\addlegendentry{Reference}
							\addplot[white] coordinates {(0, 200) (1080, 200)};
							\addlegendentry{200}
							\addplot[struct] table [x = time, y = transactions_avg, y error = transactions_std, col sep = semicolon]{CSV/default/transactions_A.csv};
							\addlegendentry{A}
							\addplot[amber] table [x = time, y = transactions_avg, y error = transactions_std, col sep = semicolon]{CSV/default/transactions_B.csv};
							\addlegendentry{B}

							\node[alert] at (axis cs: 90,880) {\xmark};
							\node[alert] at (axis cs: 270,880) {\xmark};
							\node[alert] at (axis cs: 450,880) {\xmark};
							\node[example] at (axis cs: 630,880) {\cmark};
							\node[alert] at (axis cs: 810,880) {\xmark};
							\node[example] at (axis cs: 990,880) {\cmark};

							\node[amber] at (axis cs: 90,950) {high};
							\node[amber] at (axis cs: 270,950) {low};
							\node[amber] at (axis cs: 450,950) {high};
							\node[amber] at (axis cs: 630,950) {low};
							\node[amber] at (axis cs: 810,950) {high};
							\node[amber] at (axis cs: 990,950) {high};

							\node[struct] at (axis cs: 90,1000) {high};
							\node[struct] at (axis cs: 270,1000) {high};
							\node[struct] at (axis cs: 450,1000) {high};
							\node[struct] at (axis cs: 630,1000) {low};
							\node[struct] at (axis cs: 810,1000) {low};
							\node[struct] at (axis cs: 990,1000) {none};
						\end{axis}
					\end{tikzpicture}
				\end{figure}

				\begin{itemize}
					\item<2-> Weak consolidation.
					\item<2-> No isolation.
				\end{itemize}
			\end{column}

			\begin{column}{.3\textwidth}<3->
				\centering

				\textbf{\texttt{Max} limits}

				\begin{figure}[H]
					\begin{tikzpicture}[scale = .45]
						\begin{axis}[ymax = 1100, error bars/y dir = both, xlabel = Time (seconds), ylabel = Transactions per second, legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
							\addplot[example] coordinates {(0, 800) (1080, 800)};
							\addlegendentry{Reference}
							\addplot[white] coordinates {(0, 200) (1080, 200)};
							\addlegendentry{200}
							\addplot[struct] table [x = time, y = transactions_avg, y error = transactions_std, col sep = semicolon]{CSV/max/transactions_A.csv};
							\addlegendentry{A}
							\addplot[amber] table [x = time, y = transactions_avg, y error = transactions_std, col sep = semicolon]{CSV/max/transactions_B.csv};
							\addlegendentry{B}

							\node[example] at (axis cs: 90,880) {\cmark};
							\node[alert] at (axis cs: 270,880) {\xmark};
							\node[example] at (axis cs: 450,880) {\cmark};
							\node[example] at (axis cs: 630,880) {\cmark};
							\node[alert] at (axis cs: 810,880) {\xmark};
							\node[alert] at (axis cs: 990,880) {\xmark};

							\node[amber] at (axis cs: 90,950) {high};
							\node[amber] at (axis cs: 270,950) {low};
							\node[amber] at (axis cs: 450,950) {high};
							\node[amber] at (axis cs: 630,950) {low};
							\node[amber] at (axis cs: 810,950) {high};
							\node[amber] at (axis cs: 990,950) {high};

							\node[struct] at (axis cs: 90,1000) {high};
							\node[struct] at (axis cs: 270,1000) {high};
							\node[struct] at (axis cs: 450,1000) {high};
							\node[struct] at (axis cs: 630,1000) {low};
							\node[struct] at (axis cs: 810,1000) {low};
							\node[struct] at (axis cs: 990,1000) {none};
						\end{axis}
					\end{tikzpicture}
				\end{figure}

				\begin{itemize}
					\item<4-> No consolidation.
					\item<4-> Isolation.
				\end{itemize}
			\end{column}

			\begin{column}{.3\textwidth}<5->
				\centering

				\textbf{\texttt{Soft} limits}
				\begin{figure}[H]
					\begin{tikzpicture}[scale = .45]
						\begin{axis}[ymax = 1100, error bars/y dir = both, xlabel = Time (second), ylabel = Transactions per second, legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
							\addplot[example] coordinates {(0, 800) (1080, 800)};
							\addlegendentry{Reference}
							\addplot[white] coordinates {(0, 200) (1080, 200)};
							\addlegendentry{200}
							\addplot[struct] table [x = time, y = transactions_avg, y error = transactions_std, col sep = semicolon]{CSV/soft/transactions_A.csv};
							\addlegendentry{A}
							\addplot[amber] table [x = time, y = transactions_avg, y error = transactions_std, col sep = semicolon]{CSV/soft/transactions_B.csv};
							\addlegendentry{B}

							\node[example] at (axis cs: 90,880) {\cmark};
							\node[alert] at (axis cs: 270,880) {\xmark};
							\node[example] at (axis cs: 450,880) {\cmark};
							\node[example] at (axis cs: 630,880) {\cmark};
							\node[alert] at (axis cs: 810,880) {\xmark};
							\node[example] at (axis cs: 990,880) {\cmark};

							\node[amber] at (axis cs: 90,950) {high};
							\node[amber] at (axis cs: 270,950) {low};
							\node[amber] at (axis cs: 450,950) {high};
							\node[amber] at (axis cs: 630,950) {low};
							\node[amber] at (axis cs: 810,950) {high};
							\node[amber] at (axis cs: 990,950) {high};

							\node[struct] at (axis cs: 90,1000) {high};
							\node[struct] at (axis cs: 270,1000) {high};
							\node[struct] at (axis cs: 450,1000) {high};
							\node[struct] at (axis cs: 630,1000) {low};
							\node[struct] at (axis cs: 810,1000) {low};
							\node[struct] at (axis cs: 990,1000) {none};
						\end{axis}
					\end{tikzpicture}
				\end{figure}

				\begin{itemize}
					\item<6> No consolidation.
					\item<6> Isolation.
				\end{itemize}
			\end{column}
		\end{columns}
	\end{frame}

	\begin{frame}
		\frametitle{Performance}
		\framesubtitle{Dichotomy between user and kernel vision}

		\includegraphics<1>[scale = .5]{performance-fig1.pdf}
		\includegraphics<2>[scale = .5]{performance-fig2.pdf}
		\includegraphics<3>[scale = .5]{performance-fig3.pdf}
		\includegraphics<4>[scale = .5]{performance-fig4.pdf}
		\includegraphics<5>[scale = .5]{performance-fig5.pdf}
	\end{frame}

	\begin{frame}
		\frametitle{Mechanism}
		\framesubtitle{Components}

		We want to have consolidation \textbf{and} isolation.

		\begin{columns}[T]
			\begin{column}{.3\textwidth}<2->
				\begin{enumerate}
					\item Applicative probe (\texttt{perl script})
					\item In kernel mechanism
				\end{enumerate}
			\end{column}
			\begin{column}{.7\textwidth}
				\begin{figure}[H]
					\centering

					\includegraphics<3>[scale = .15]{mechanism-fig1.pdf}
					\includegraphics<4>[scale = .15]{mechanism-fig2.pdf}
					\includegraphics<5>[scale = .15]{mechanism-fig3.pdf}
					\includegraphics<6>[scale = .15]{mechanism-fig4.pdf}
					\includegraphics<7>[scale = .15]{mechanism-fig5.pdf}
				\end{figure}
			\end{column}
		\end{columns}
	\end{frame}

	\begin{frame}
		\frametitle{Experiments}
		\framesubtitle{Mechanism}

		\begin{columns}
			\begin{column}{.5\textwidth}<1->
				\textbf{Our mechanism}
				\begin{figure}[H]
					\begin{tikzpicture}[scale = .5]
						\begin{axis}[ymax = 1100, error bars/y dir = both, xlabel = Time (second), ylabel = Transactions per second, legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
							\addplot[example] coordinates {(0, 800) (1080, 800)};
							\addlegendentry{Reference}
							\addplot[white] coordinates {(0, 200) (1080, 200)};
							\addlegendentry{200}
							\addplot[struct] table [x = time, y = transactions_avg, y error = transactions_std, col sep = semicolon]{CSV/mechanism/transactions_A.csv};
							\addlegendentry{A}
							\addplot[amber] table [x = time, y = transactions_avg, y error = transactions_std, col sep = semicolon]{CSV/mechanism/transactions_B.csv};
							\addlegendentry{B}

							\node[example] at (axis cs: 90,880) {\cmark};
							\node[example] at (axis cs: 270,880) {\cmark};
							\node[example] at (axis cs: 450,880) {\cmark};
							\node[example] at (axis cs: 630,880) {\cmark};
							\node[example] at (axis cs: 810,880) {\cmark};
							\node[example] at (axis cs: 990,880) {\cmark};

							\node[amber] at (axis cs: 90,950) {high};
							\node[amber] at (axis cs: 270,950) {low};
							\node[amber] at (axis cs: 450,950) {high};
							\node[amber] at (axis cs: 630,950) {low};
							\node[amber] at (axis cs: 810,950) {high};
							\node[amber] at (axis cs: 990,950) {high};

							\node[struct] at (axis cs: 90,1000) {high};
							\node[struct] at (axis cs: 270,1000) {high};
							\node[struct] at (axis cs: 450,1000) {high};
							\node[struct] at (axis cs: 630,1000) {low};
							\node[struct] at (axis cs: 810,1000) {low};
							\node[struct] at (axis cs: 990,1000) {none};
						\end{axis}
					\end{tikzpicture}
				\end{figure}
			\end{column}

			\begin{column}{.5\textwidth}<2>
				\textbf{\texttt{Soft} limits}
				\begin{figure}[H]
					\begin{tikzpicture}[scale = .5]
						\begin{axis}[ymax = 1100, error bars/y dir = both, xlabel = Time (second), ylabel = Transactions per second, legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
							\addplot[example] coordinates {(0, 800) (1080, 800)};
							\addlegendentry{Reference}
							\addplot[white] coordinates {(0, 200) (1080, 200)};
							\addlegendentry{200}
							\addplot[struct] table [x = time, y = transactions_avg, y error = transactions_std, col sep = semicolon]{CSV/soft/transactions_A.csv};
							\addlegendentry{A}
							\addplot[amber] table [x = time, y = transactions_avg, y error = transactions_std, col sep = semicolon]{CSV/soft/transactions_B.csv};
							\addlegendentry{B}

							\node[example] at (axis cs: 90,880) {\cmark};
							\node[alert] at (axis cs: 270,880) {\xmark};
							\node[example] at (axis cs: 450,880) {\cmark};
							\node[example] at (axis cs: 630,880) {\cmark};
							\node[alert] at (axis cs: 810,880) {\xmark};
							\node[example] at (axis cs: 990,880) {\cmark};

							\node[amber] at (axis cs: 90,950) {high};
							\node[amber] at (axis cs: 270,950) {low};
							\node[amber] at (axis cs: 450,950) {high};
							\node[amber] at (axis cs: 630,950) {low};
							\node[amber] at (axis cs: 810,950) {high};
							\node[amber] at (axis cs: 990,950) {high};

							\node[struct] at (axis cs: 90,1000) {high};
							\node[struct] at (axis cs: 270,1000) {high};
							\node[struct] at (axis cs: 450,1000) {high};
							\node[struct] at (axis cs: 630,1000) {low};
							\node[struct] at (axis cs: 810,1000) {low};
							\node[struct] at (axis cs: 990,1000) {none};
						\end{axis}
					\end{tikzpicture}
				\end{figure}
			\end{column}
		\end{columns}
	\end{frame}

	\begin{frame}
		\frametitle{Memory footprints}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\textbf{Our mechanism}

				\begin{tikzpicture}[scale = .5]
					\begin{axis}[error bars/y dir = both, xlabel = Time (second), ylabel = Memory (byte), legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
						\addplot[example] coordinates {(0, 2.8 * 10^9) (1080, 2.8 * 10^9)};
						\addlegendentry{Reference}
						\addplot[ppink] coordinates {(0, 18 * 10^8) (1080, 18 * 10^8)};
						\addlegendentry{A's \texttt{soft} limit}
						\addplot[ppink, dotted] coordinates {(0, 10 * 10^8) (1080, 10 * 10^8)};
						\addlegendentry{B's \texttt{soft} limit}
						\addplot[struct] table [x = iteration, y = usage_avg, y error = usage_std, col sep = semicolon]{CSV/mechanism/stats_A.csv};
						\addlegendentry{A}
						\addplot[amber] table [x = iteration, y = usage_avg, y error = usage_std, col sep = semicolon]{CSV/mechanism/stats_B.csv};
						\addlegendentry{B}
					\end{axis}
				\end{tikzpicture}
			\end{column}

			\begin{column}{.5\textwidth}
				\textbf{\texttt{Soft} mechanism}
				\begin{tikzpicture}[scale = .5]
					\begin{axis}[error bars/y dir = both, xlabel = Time (second), ylabel = Reads from disk, legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
						\addplot[example] coordinates {(0, 2.8 * 10^9) (1080, 2.8 * 10^9)};
						\addlegendentry{Reference}
						\addplot[ppink] coordinates {(0, 18 * 10^8) (1080, 18 * 10^8)};
						\addlegendentry{A's \texttt{soft} limit}
						\addplot[ppink, dotted] coordinates {(0, 10 * 10^8) (1080, 10 * 10^8)};
						\addlegendentry{B's \texttt{soft} limit}
						\addplot[struct] table [x = iteration, y = usage_avg, y error = usage_std, col sep = semicolon]{CSV/soft/stats_A.csv};
						\addlegendentry{A}
						\addplot[amber] table [x = iteration, y = usage_avg, y error = usage_std, col sep = semicolon]{CSV/soft/stats_B.csv};
						\addlegendentry{B}
					\end{axis}
				\end{tikzpicture}
			\end{column}
		\end{columns}
	\end{frame}

	\begin{frame}
		\frametitle{Mechanism performance}

		\begin{columns}
			\begin{column}{.3\textwidth}<1->
				\begin{tikzpicture}[scale = .45]
					\begin{axis}[xlabel = Time (in second), ylabel = Number of call, legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
						\addplot[white] coordinates {(60, 0) (60, 150)};
						\addlegendentry{Begin time}
						\addplot[violet_profond] table [x = time, y = calls, col sep = semicolon]{CSV/soft/calls.csv};
						\addlegendentry{\texttt{Soft} limit}
						\addplot[sarcelle] table [x = time, y = calls, col sep = semicolon]{CSV/mechanism/calls.csv};
						\addlegendentry{Our mechanism}
					\end{axis}
				\end{tikzpicture}
			\end{column}

			\begin{column}{.3\textwidth}<2->
				\begin{tikzpicture}[scale = .45]
					\begin{axis}[symbolic x coords = {Calls, Time}, xtick = data, ylabel = Number of calls, ybar = 0, legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
						\addplot [color=violet_profond, fill] coordinates{
							(Calls, 17231.8)
							(Time, 0)
						};

						\addplot [color=sarcelle, fill] coordinates{
							(Calls, 11095.3)
							(Time, 0)
						};

						\legend{\texttt{Soft} limit, Our mechanism}
					\end{axis}

					\begin{axis}[axis x line = none, axis y line* = right, ylabel style = {align=center}, symbolic x coords = {Calls, Time}, xtick = data, ybar = 0, legend = none]
						\addplot [color=violet_profond, fill] coordinates{
							(Calls, 0)
							(Time, 2.003918)
						};

						\addplot [color=sarcelle, fill] coordinates{
							(Calls, 0)
							(Time, 3.458549)
						};
					\end{axis}
				\end{tikzpicture}
			\end{column}

			\begin{column}{.3\textwidth}<3>
				\begin{tikzpicture}[scale = .45]
					\begin{axis}[xticklabels = \empty, ymin = 0, ybar, bar width = 28pt, ylabel = Time (in seconds), legend style = {at = {(0.5,-0.1)}, anchor = north, legend columns = -1, fill = none}]
						\addplot[violet_profond, fill] table [x expr = \coordindex, y = system_time, col sep = semicolon]{CSV/soft/time.csv};
						\addlegendentry{\texttt{Soft} limit}
						\addplot[sarcelle, fill] table [x expr = \coordindex, y = system_time, col sep = semicolon]{CSV/mechanism/time.csv};
						\addlegendentry{Our mechanism}
					\end{axis}
				\end{tikzpicture}
			\end{column}
		\end{columns}
	\end{frame}

	\begin{frame}
		\frametitle{Conclusion}
		\framesubtitle{Observation}

		\begin{itemize}
			\item Container permit resource limitation and application isolation.
			\item They offer memory isolation.
			\item But memory consolidation is imperfect.
			\item Our mechanism enables memory consolidation for \texttt{soft} limit.
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Future work}

		\begin{columns}[T]
			\begin{column}{.3\textwidth}
				\begin{itemize}
					\item Run with more containers.
					\item Modify the interesting set of records for databases.
					\item Run with other benchmarks.
				\end{itemize}
			\end{column}
			\begin{column}{.7\textwidth}<2->
				Try to estimate container working set size.

				\begin{figure}[H]
					\centering

					\includegraphics<2>[scale = .5]{working_set_estimation-fig1.pdf}
					\includegraphics<3>[scale = .5]{working_set_estimation-fig2.pdf}
				\end{figure}
			\end{column}
		\end{columns}
	\end{frame}

	\begin{frame}
		\frametitle{Timeline}

		\begin{itemize}
			\item Year 1: First topic:
			\begin{itemize}
				\item State of the art on memory functionning, NVRAM and memory power consumption.
				\item Experiment on memory power consumption.
			\end{itemize}
			\item Year 2: Second topic:
			\begin{itemize}
				\item Take charge of docker.
				\item State of the art on memory consolidation, auto-scaling and working set estimation.
				\item Development of the mechanism and experiment to test it.
				\item Try to submit a paper to NCA conference.
			\end{itemize}
			\item Year 3:
			\begin{itemize}
				\item Future works.
				\item Thesis defense.
			\end{itemize}
		\end{itemize}
	\end{frame}

	\begin{frame}[allowframebreaks, noframenumbering]
		\frametitle{Bibliography}
		\setbeamertemplate{bibliography item}[text]

		\begin{scriptsize}
			\bibliographystyle{acm}
			\bibliography{bibliography}

			\bigskip
			Pictures designed by \href{https://www.flaticon.com/authors/smashicons}{Smashicons}, \href{https://www.flaticon.com/authors/good-ware}{Good Ware}, \href{https://icon54.com/}{Icons mind}, \href{https://www.flaticon.com/authors/simpleicon}{SimpleIcon} and \href{http://www.freepik.com}{Freepik} from \href{http://www.flaticon.com}{Flaticon}.
		\end{scriptsize}
	\end{frame}

	\begin{frame}[noframenumbering]
		\frametitle{Experiments}
		\framesubtitle{Vanilla: memory}

		\begin{tikzpicture}
			\begin{axis}[error bars/y dir = both, xlabel = Time (second), ylabel = Memory (byte), legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
				\addplot[example] coordinates {(0, 2.8 * 10^9) (1080, 2.8 * 10^9)};
				\addlegendentry{Reference}
				\addplot[struct] table [x = iteration, y = usage_avg, y error = usage_std, col sep = semicolon]{CSV/default/stats_A.csv};
				\addlegendentry{A}
				\addplot[amber] table [x = iteration, y = usage_avg, y error = usage_std, col sep = semicolon]{CSV/default/stats_B.csv};
				\addlegendentry{B}
			\end{axis}
		\end{tikzpicture}
	\end{frame}

	\begin{frame}[noframenumbering]
		\frametitle{Experiments}
		\framesubtitle{Vanilla: reads from disk}

		\begin{tikzpicture}
			\begin{axis}[error bars/y dir = both, xlabel = Time (second), ylabel = Reads from disk, legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
				\addplot[example] coordinates {(0, 100) (1080, 100)};
				\addlegendentry{Reference}
				\addplot[struct] table [x = iteration, y = reads_avg, y error = reads_std, col sep = semicolon]{CSV/default/stats_A.csv};
				\addlegendentry{A}
				\addplot[amber] table [x = iteration, y = reads_avg, y error = reads_std, col sep = semicolon]{CSV/default/stats_B.csv};
				\addlegendentry{B}
			\end{axis}
		\end{tikzpicture}
	\end{frame}

	\begin{frame}[noframenumbering]
		\frametitle{Experiments}
		\framesubtitle{Max limit: memory}

		\begin{tikzpicture}
			\begin{axis}[error bars/y dir = both, xlabel = Time (second), ylabel = Memory (byte), legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
				\addplot[example] coordinates {(0, 2.8 * 10^9) (1080, 2.8 * 10^9)};
				\addlegendentry{Reference}
				\addplot[ppink] coordinates {(0, 18 * 10^8) (1080, 18 * 10^8)};
				\addlegendentry{A's \texttt{max} limit}
				\addplot[ppink] coordinates {(0, 10 * 10^8) (1080, 10 * 10^8)};
				\addlegendentry{B's \texttt{max} limit}
				\addplot[struct] table [x = iteration, y = usage_avg, y error = usage_std, col sep = semicolon]{CSV/max/stats_A.csv};
				\addlegendentry{A}
				\addplot[amber] table [x = iteration, y = usage_avg, y error = usage_std, col sep = semicolon]{CSV/max/stats_B.csv};
				\addlegendentry{B}
			\end{axis}
		\end{tikzpicture}
	\end{frame}

	\begin{frame}[noframenumbering]
		\frametitle{Experiments}
		\framesubtitle{Max limit: reads from disk}

		\begin{tikzpicture}
			\begin{axis}[error bars/y dir = both, xlabel = Time (second), ylabel = Reads from disk, legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
				\addplot[example] coordinates {(0, 100) (1080, 100)};
				\addlegendentry{Reference}
				\addplot[struct] table [x = iteration, y = reads_avg, y error = reads_std, col sep = semicolon]{CSV/max/stats_A.csv};
				\addlegendentry{A}
				\addplot[amber] table [x = iteration, y = reads_avg, y error = reads_std, col sep = semicolon]{CSV/max/stats_B.csv};
				\addlegendentry{B}
			\end{axis}
		\end{tikzpicture}
	\end{frame}

	\begin{frame}[noframenumbering]
		\frametitle{Experiments}
		\framesubtitle{Soft limit: reads from disk}

		\begin{tikzpicture}
			\begin{axis}[error bars/y dir = both, xlabel = Time (second), ylabel = Reads from disk, legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
				\addplot[example] coordinates {(0, 100) (1080, 100)};
				\addlegendentry{Reference}
				\addplot[struct] table [x = iteration, y = reads_avg, y error = reads_std, col sep = semicolon]{CSV/soft/stats_A.csv};
				\addlegendentry{A}
				\addplot[amber] table [x = iteration, y = reads_avg, y error = reads_std, col sep = semicolon]{CSV/soft/stats_B.csv};
				\addlegendentry{B}
			\end{axis}
		\end{tikzpicture}
	\end{frame}

	\begin{frame}[noframenumbering]
		\frametitle{Experiments}
		\framesubtitle{Mechanism: reads from disk}

		\begin{tikzpicture}
			\begin{axis}[error bars/y dir = both, xlabel = Time (second), ylabel = Reads from disk, legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
				\addplot[example] coordinates {(0, 100) (1080, 100)};
				\addlegendentry{Reference}
				\addplot[struct] table [x = iteration, y = reads_avg, y error = reads_std, col sep = semicolon]{CSV/mechanism/stats_A.csv};
				\addlegendentry{A}
				\addplot[amber] table [x = iteration, y = reads_avg, y error = reads_std, col sep = semicolon]{CSV/mechanism/stats_B.csv};
				\addlegendentry{B}
			\end{axis}
		\end{tikzpicture}
	\end{frame}
\end{document}